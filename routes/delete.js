require('dotenv').config();
let express = require('express');
let route = express.Router();
let app = express();
app.use(express.json());
let mysql = require('mysql2/promise');

let user = process.env.USER;
let host = process.env.HOST;
let passweord = process.env.PASSWORD;
let database = process.env.DATABASE;
let db;
async function creatDatabase() {
    db = await mysql.createConnection({
        user: user,
        host: host,
        password: passweord,
        database: database
    })
};
creatDatabase();

route.delete('/produce/:id', async (req, res) => {
    let id = req.params.id;
    let autho = req.headers.authorization;
    let authosql = 'select user_id,u.name,`key`,produce.id,produce.name from produce join user u on u.id = produce.user_id where produce.id=? and u.key=?'
    let deletesql = 'DELETE from produce WHERE id=?';
    try {
        let [row, filed] = await db.query(authosql, [id, autho])
        if (row.length == 0) {
            res.status(401).json({massage:'Incorrect Authorization key'})
        }
        else{
            let [result, data] = await db.query(deletesql, [id]);
            
                res.json({ result, massage: 'delete successfull' });
                // console.log(result.affectedRows);
            }
          
    
        
      
        }
        catch (error) {
            res.send(error)
        }

})
module.exports = route