require('dotenv').config();
let express = require('express');
let route = express.Router();
let app = express();
app.use(express.json());
let mysql = require('mysql2/promise');

let user = process.env.USER;
let host = process.env.HOST;
let passweord = process.env.PASSWORD;
let database = process.env.DATABASE;
let db;
async function creatDatabase() {
    db = await mysql.createConnection({
        user: user,
        host: host,
        password: passweord,
        database: database
    })
};
creatDatabase();

route.post('/produce', async (req, res) => {
    let name = req.body.name;
    let type = req.body.type;
    let variety = req.body.variety;
    let color = req.body.color;
    let user = req.body.user_id;
    console.log(req.body);
    let autho = req.headers.authorization;
    let authosql = 'select user_id,u.name,`key`,produce.id,produce.name from produce join user u on u.id = produce.user_id where u.key=? and user_id=?'
    let postsql = "INSERT INTO produce  (name, type, variety, color, user_id) VALUES (?, ?, ?, ?, ?)";
    try {
        let [row, filed] = await db.query(authosql, [autho, user])
        if (row.length == 0) {
            res.status(401).json({ massage: 'Incorrect Authorization key' })
        }
        else {
            let [result, data] = await db.query(postsql, [name, type, variety, color, user]);
            res.json({ result, massage: 'add successfully' });
        }

    }
    catch (error) {
        res.send(error);
    }

});
module.exports = route