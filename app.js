let login= require('./routes/login')
let post =require('./routes/post');
let put = require('./routes/put');
let remove= require('./routes/delete');
let get=require('./routes/get');
let express= require('express');
let app = express();
app.use(express.json());
let port = process.env.PORT;

app.use(login);
app.use(post);
app.use(put);
app.use(remove);
app.use(get);

app.listen(port,()=>{
    console.log('server run');
})